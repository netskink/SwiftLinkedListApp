//
//  LinkedList.swift
//  SwiftLinkedListApp
//
//  Created by John F. Davis on 3/9/16.
//  Copyright © 2016 John F. Davis. All rights reserved.
//

import Foundation

class Node<T> {

    var value : T?
    
    var next : Node?
    
    
    init() {
        next = nil;
        value = nil;
    }
}


class LinkedList<T> {
    
    var head : Node<T>?
    var tail : Node<T>?
    var count : Int
    
    init() {
        head = nil;
        tail = nil;
        count = 0;
    }
    
    func append(value: T)  {
        let aNode = Node<T>();
        aNode.value = value;
        
        if (nil == head) {
            self.head = aNode;
            self.tail = aNode;
        } else {
            self.tail!.next = aNode;
            self.tail = aNode;
            
        }
        
        count++;
    }
    
    
    
    
    func reverse() {

        var p : Node<T>?;
        var q : Node<T>?;
        var r : Node<T>?;

        p = head;
        q = nil;
        r = nil;

        tail = head

        while (p != nil) {
            r = q;
            q = p;
            p = p!.next;
            q!.next = r;
        }

        head = q
        
    }

    
}