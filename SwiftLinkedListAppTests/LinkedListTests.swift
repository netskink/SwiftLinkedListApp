//
//  LinkedList.swift
//  SwiftLinkedListApp
//
//  Created by John F. Davis on 3/9/16.
//  Copyright © 2016 John F. Davis. All rights reserved.
//

// Importing XCTest because of XCTestCase
import XCTest

// Importing AppKit because of NSApplication
import AppKit

// Importing MySwiftApp because of AppDelegate
@testable import SwiftLinkedListApp




class LinkedListTests: XCTestCase {
    
    
    

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testReverse() {
        
        let linkedList = LinkedList<Int>();
        
        // Create a list with 4 items.
        for (var i=0;i<4;i++) {
            linkedList.append(i);
        }

        // Dump the items using nil check and check log
        print("what we have at start");
        var node = linkedList.head;
        while(node != nil) {
            print(node!.value!);
            node = node!.next;
        }
        
        
        // Fulll Rotate
        linkedList.reverse()
        
        // Dump the items using nil check and check log
        print("What we have after reverse");
        node = linkedList.head;
        while(node != nil) {
            print(node!.value!);
            node = node!.next;
        }
        
        
    }
    
    
    
    func testSimpleAdd() {
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        // Test if we can create an empty linkedList
        let linkedList = LinkedList<Int>();
        XCTAssert(linkedList.count == 0, "The count was not zero");
        
        
        // Test if we can add a single item
        linkedList.append(0);
        XCTAssert(linkedList.count == 1, "The count was not one");
        
        // Test if we can add additional items
        for (var i=0;i<10;i++) {
            linkedList.append(i+1);
        }
        XCTAssert(linkedList.count == 11, "The count was not eleven");
        
        // Dump the items using nil check and check log
        var node = linkedList.head;
        while(node != nil) {
            print(node!.value!);
            node = node!.next;
        }
        
    }



}
